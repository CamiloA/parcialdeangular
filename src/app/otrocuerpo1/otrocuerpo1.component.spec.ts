import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Otrocuerpo1Component } from './otrocuerpo1.component';

describe('Otrocuerpo1Component', () => {
  let component: Otrocuerpo1Component;
  let fixture: ComponentFixture<Otrocuerpo1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Otrocuerpo1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Otrocuerpo1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
